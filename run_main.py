# parser_args code referred the hwalseoklee's code:
# https://github.com/hwalsuklee/tensorflow-mnist-VAE/blob/master/run_main.py
import tensorflow as tf
from utils import loaders, plot
from model.autoencoder import VAE
from train_utils.autoencodertrain import VAETrain
import time
import argparse


def parse_args():
    desc = "Tensorflow 2.0 implementation of 'AutoEncoder Families (AE, VAE, CVAE(Conditional VAE))'"
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('--epochs', type=int, default=100,
                        help='The number of training epochs')
    parser.add_argument('--lr', type=float, default=1e-4,
                        help='Learning rate during training')
    parser.add_argument('--batch_size', type=int, default=1000,
                        help='Batch size')
    parser.add_argument('--img_src', type=str, default='/gdrive/My Drive/schottky/PNG/original',
                        help='directory containing original images')
    parser.add_argument('--loss_type', type=str, default='mse',
                        help='type of reconstruction loss')
    parser.add_argument('--beta', type=float, default=1,
                        help='beta value for loss weighting')
    parser.add_argument('--in_shape', type=tuple, default=(1024, 1024, 1),
                        help='shape of input data frame')
    # parser.add_argument('--apply_sigmoid', type=str, default='True',
    #                     help='apply sigmoid function to decoder output')
    parser.add_argument('--no_sigmoid', dest='no_sigmoid', default=False, action='store_true',
                        help='do not apply sigmoid function to decoder output')

    return parser.parse_args()


def train_VAE(epochs, lr, batch_size, img_src, loss_type, beta, in_shape, apply_sigmoid):

    model = VAE(
          in_shape=in_shape
        , encoder_conv_filters=[32, 64, 64, 128, 128, 256, 256, 512]
        , encoder_conv_kernel_size=[3, 3, 3, 3, 3, 3, 3, 3]
        , encoder_conv_strides=[2, 2, 2, 2, 2, 2, 2, 2]
        , decoder_conv_t_filters=[512, 256, 256, 128, 128, 64, 32, 1]
        , decoder_conv_t_kernel_size=[3, 3, 3, 3, 3, 3, 3, 3]
        , decoder_conv_t_strides=[2, 2, 2, 2, 2, 2, 2, 2]
        , z_dim=16
        , dropout=0.2
        , use_batch_norm=False
        )

    loss_hist = []
    train_dataset, test_dataset = loaders.load_schottky_dataset(img_src, in_shape, batch_size=batch_size)

    optimizer = tf.keras.optimizers.Adam(lr)

    for epoch in range(1, epochs + 1):
        t = time.time()
        for train_x, _ in train_dataset:
            gradients, losses = VAETrain.compute_gradients(model, train_x, loss_type, beta, apply_sigmoid)
            VAETrain.apply_gradients(optimizer, gradients, model.trainable_variables)
            loss_hist.append([losses[1],losses[2]])
        if epoch % 10 == 0:
            print('Epoch {}, Loss: {}, Remaining Time at This Epoch: {:.2f}'.format(
                epoch, loss_hist[-1], time.time() - t
            ))

    plot.plot_VAE(model, test_dataset, in_shape, loss_hist, apply_sigmoid)

    return model


def main(args):

    train_VAE(
        epochs=args.epochs,
        lr=args.lr,
        batch_size=args.batch_size,
        img_src = args.img_src,
        loss_type = args.loss_type,
        beta = args.beta,
        in_shape = args.in_shape,
        apply_sigmoid = not(args.no_sigmoid)
    )


if __name__ == "__main__":
    args = parse_args()
    main(args)
