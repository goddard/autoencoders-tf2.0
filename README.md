# Auto-Encoders for Schotty spectra using TF 2.0
Implementation of betaVAE (beta-Variational AE)

Typical beta values in range 0.01 - 100, depends on reconstruction loss type

## Important note ##

The loss functions are normalised **per pixel** (for the reconstruction loss) and **per z dimension** (for the KL divergence). The beta factor is then applied to the KL divergence term. In this way, the different reconstruction loss types can be directly compared without tuning beta, and also the VAE performance should be independant of changing image size and of changing z dimensionality.

## Results

### VAE(Variational Auto-Encoder)

```
python run_main.py --z_dim 16 --epochs 1000 --dropout 0.2 --loss_type <xent_logits, xent, mse> --beta 0.1 --batch_size 64
--lr 0.001 --img_src <path> --no_sigmoid
```

<table>
<tr>
<td>Original (top) and Reconsructed Images</td>
<td><img src='results/VAE_reconstruction.png'></td>
</tr>
<tr>
<td>Distribution of z</td>
<td><img src='results/VAE_distribution.png' height=500></td>
</tr>
</table>


## More Conceptual Plots

### VAE

Visualizations of generated images from (z1, z2) ∈ [-2, 2] uniform distribution (other z's are random.normal).

![](results/VAE_conceptual.png)

## Usage

### Prerequisites

1. `tensorflow 2.0.0-alpha0` or higher
2. python packages: `numpy`, `matplotlib`

### Command

```
python run_main.py
```

_Example:_ `python run main.p --z_dim 16 --epochs 10000

### Arguments

_Optional:_

- `--z_dim`: Dimension of latent vector(z). _Default:_ `2`
- `--epochs`: The number of epochs to run. _Default:_ `100`
- `--batch_size`: The size of batch. _Default:_ `1000`
- `--lr`: Learning rate of Adam optimizer. _Default:_ `1e-4`
- `--dropout` : dropout. _Default:_ `0.2`
- `--loss_type` : loss type. _Default:_ `mse`
- `--beta` : KL loss weight. _Default:_ `0.1`
- `--img_src` : path to folder containing images.
- `--no_sigmoid` : switches off decoder sigmoid activation. _Default:_ `False`
```

## References
[1] https://github.com/hwalsuklee/tensorflow-mnist-VAE  
[2] https://www.slideshare.net/NaverEngineering/ss-96581209  
[3] https://www.tensorflow.org/alpha/tutorials/generative/cvae  
