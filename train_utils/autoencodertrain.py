import tensorflow as tf

class VAETrain:
        
    @staticmethod
    def compute_loss(model, x, loss_type, beta, apply_sigmoid):
        assert loss_type in ['xent', 'xent_logits', 'mse']

        norm = x.shape[1] * x.shape[2] #normalization factor (total pixels) for MSE and xent loss terms
        mean, logvar = model.encode(x)
        z = model.reparameterize(mean, logvar)
        x_decoded = model.decode(z, apply_sigmoid)
        
        KL_divergence = tf.reduce_sum(mean ** 2 + tf.exp(logvar) - logvar - 1, axis=1)
        KL_divergence = tf.reduce_mean(KL_divergence)

        if loss_type == "xent_logits":
            # cross_ent = - marginal likelihood
            # note that for binarized data, 'apply sigmoid' should be True in VAE.decode
            cross_ent = tf.nn.sigmoid_cross_entropy_with_logits(logits=x_decoded, labels=x)
            marginal_likelihood = - tf.reduce_sum(cross_ent, axis=[1, 2, 3])
            marginal_likelihood = tf.reduce_mean(marginal_likelihood)
            ELBO = marginal_likelihood - beta * KL_divergence
            loss = -ELBO
            r_loss = -marginal_likelihood
            
        elif loss_type == "xent":
            bce = tf.keras.losses.BinaryCrossentropy()
            loss = norm * bce(x, x_decoded)
            r_loss = loss
            loss += beta * KL_divergence

        else:
            mse = tf.losses.MeanSquaredError()
            loss = norm * mse(x_decoded, x)
            r_loss = loss
            loss += beta * KL_divergence

        return (loss, r_loss.numpy(), KL_divergence.numpy())

    @staticmethod
    def compute_gradients(model, x, loss_type, beta, apply_sigmoid):
        with tf.GradientTape() as tape:
            losses  = VAETrain.compute_loss(model, x, loss_type, beta, apply_sigmoid)
        return tape.gradient(losses[0], model.trainable_variables), losses

    @staticmethod
    def apply_gradients(optimizer, gradients, variables):
        optimizer.apply_gradients(zip(gradients, variables))