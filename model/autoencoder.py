import tensorflow as tf
from tensorflow.keras.layers import Input, Conv2D, Flatten, Dense, Dropout, Conv2DTranspose, Reshape, LeakyReLU, BatchNormalization

class VAE(tf.keras.Model):
    def __init__(self
                 , in_shape
                 , encoder_conv_filters
                 , encoder_conv_kernel_size
                 , encoder_conv_strides
                 , decoder_conv_t_filters
                 , decoder_conv_t_kernel_size
                 , decoder_conv_t_strides
                 , z_dim
                 , dropout
                 , use_batch_norm
                 ):

            super(VAE, self).__init__()

            self.in_shape = in_shape
            self.encoder_conv_filters = encoder_conv_filters
            self.encoder_conv_kernel_size = encoder_conv_kernel_size
            self.encoder_conv_strides = encoder_conv_strides
            self.decoder_conv_t_filters = decoder_conv_t_filters
            self.decoder_conv_t_kernel_size = decoder_conv_t_kernel_size
            self.decoder_conv_t_strides = decoder_conv_t_strides
            self.z_dim = z_dim
            self.dropout = dropout
            self.use_batch_norm = use_batch_norm

            self.n_layers_encoder = len(encoder_conv_filters)
            self.n_layers_decoder = len(decoder_conv_t_filters)

            #super(VAE, self).__init__()

            self._build()

    def _build(self):

        ### THE ENCODER
        print('assembling the inference net')
        print('input shape : ', self.in_shape)
        encoder_input = Input(shape=self.in_shape, name='encoder_input')

        x = encoder_input

        for i in range(self.n_layers_encoder):
            conv_layer = Conv2D(
                  filters=self.encoder_conv_filters[i]
                , kernel_size=self.encoder_conv_kernel_size[i]
                , strides=self.encoder_conv_strides[i]
                , padding='same'
                , name='encoder_conv_' + str(i)
            )

            x = conv_layer(x)

            if self.use_batch_norm:
                x = BatchNormalization()(x)

            x = LeakyReLU()(x)

            if self.dropout > 0:
                x = Dropout(rate=self.dropout)(x)

        _shape_before_flattening = x.shape[1:]
        print("shape before flattening : ",_shape_before_flattening)
        x = Flatten()(x)
        x = Dense(self.z_dim * 2)(x)  # [means, stds]

        self.inference_net = tf.keras.Model(encoder_input, x)

        ### THE DECODER
        print('assembling the generative net')
        decoder_input = Input(shape=(self.z_dim,), name='decoder_input')

        x = Dense(tf.math.reduce_prod(_shape_before_flattening))(decoder_input)
        x = Reshape(_shape_before_flattening)(x)

        for i in range(self.n_layers_decoder):
            conv_t_layer = Conv2DTranspose(
                filters=self.decoder_conv_t_filters[i]
                , kernel_size=self.decoder_conv_t_kernel_size[i]
                , strides=self.decoder_conv_t_strides[i]
                , padding='same'
                , name='decoder_conv_t_' + str(i)
            )

            x = conv_t_layer(x)

            if i < self.n_layers_decoder - 1:
                if self.use_batch_norm:
                    x = BatchNormalization()(x)
                x = LeakyReLU()(x)
                if self.dropout > 0:
                    x = Dropout(rate=self.dropout)(x)

        self.generative_net = tf.keras.Model(decoder_input, x)

    def encode(self, x):
        mean_logvar = self.inference_net(x)
        N = mean_logvar.shape[0]
        mean = tf.slice(mean_logvar, [0, 0], [N, self.z_dim])
        logvar = tf.slice(mean_logvar, [0, self.z_dim], [N, self.z_dim])
        return mean, logvar

    def decode(self, z, apply_sigmoid):
        x_decoded = self.generative_net(z)
        if apply_sigmoid:
            probs = tf.sigmoid(x_decoded)
            return probs
        return x_decoded

    def reparameterize(self, mean, logvar):
        eps = tf.random.normal(shape=mean.shape)
        return eps * tf.exp(logvar * .5) + mean
