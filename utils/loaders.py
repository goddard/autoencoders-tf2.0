import tensorflow as tf
import os
import cv2
from glob import glob
import numpy as np
import imageio


def normalize(train_images, test_images):
    # Normalizing the images to the range of [0., 1.]
    train_images /= 255.
    test_images /= 255.
    return train_images, test_images


def download_schottky_images(mydir, img_shape):
    fn = os.listdir(mydir)
    filepathnames = np.array(glob(os.path.join(mydir, '*.png')))
    images = np.zeros((len(filepathnames), img_shape[0], img_shape[1],1)).astype('float32')
    for i, fpn in enumerate(filepathnames):
        im = imageio.imread(fpn)
        im_crop = im[41:-62, 90:-154, 0]  # empirical to get rid of images axes etc.
        im_resized = cv2.resize(im_crop, (img_shape[0], img_shape[1]), interpolation=cv2.INTER_AREA)
        images[i,:,:,0] = im_resized
    labels = np.ones((images.shape[0]))
    split = int(0.8*images.shape[0])
    train_images = images[0:split,:,:]
    test_images = images[split:-1,:,:]
    train_labels = labels[0:split]
    test_labels = labels[split:-1]
    return (train_images, train_labels), (test_images, test_labels)


def load_schottky_dataset(mydir, img_shape, batch_size=16):
    (train_images, train_labels), (test_images, test_labels) = download_schottky_images(mydir, img_shape)
    train_images, test_images = normalize(train_images, test_images)

    TRAIN_BUF = 64
    TEST_BUF = 8

    BATCH_SIZE = batch_size

    train_dataset_image = tf.data.Dataset.from_tensor_slices(train_images).batch(BATCH_SIZE)
    train_dataset_label = tf.data.Dataset.from_tensor_slices(train_labels).batch(BATCH_SIZE)
    train_dataset = tf.data.Dataset.zip((train_dataset_image, train_dataset_label)).shuffle(TRAIN_BUF)

    test_dataset_image = tf.data.Dataset.from_tensor_slices(test_images).batch(BATCH_SIZE)
    test_dataset_label = tf.data.Dataset.from_tensor_slices(test_labels).batch(BATCH_SIZE)
    test_dataset = tf.data.Dataset.zip((test_dataset_image, test_dataset_label)).shuffle(TEST_BUF)

    return train_dataset, test_dataset
